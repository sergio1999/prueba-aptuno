import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  datos = {
    nombres:'',
    email:'',
    telefono:''
  };
  
  constructor(){}

  ngOnInit(){}

  submitForm(){
    alert('su nombre:'+ this.datos.nombres + ', su correo: ' + this.datos.email + ', y su telefono: '+ this.datos.telefono)
  }
}
